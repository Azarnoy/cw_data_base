package com.example.azzarnoy.cw_bd;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.azzarnoy.cw_bd.database.DataBaseMaster;
import com.example.azzarnoy.cw_bd.models.User;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        User user = new User();
        user.name = "vasya";

        DataBaseMaster.getInstance(this).addUser(user);

        List<User> query = DataBaseMaster.getInstance(this).getAllUsersQuery();
        Log.d("query", "size: " + query.size());

        List<User> raw = DataBaseMaster.getInstance(this).getAllUsersRAW();
        Log.d("query", "size: " + query.size());



    }

}
