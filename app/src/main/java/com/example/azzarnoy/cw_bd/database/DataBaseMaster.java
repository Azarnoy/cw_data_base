package com.example.azzarnoy.cw_bd.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.azzarnoy.cw_bd.models.User;

import java.util.ArrayList;
import java.util.List;


public class DataBaseMaster {
    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;

    private static DataBaseMaster instance;

    private DataBaseMaster(Context context) {
        dbCreator = new DataBaseCreator(context);
        if (database == null || !database.isOpen()) {
            database = dbCreator.getWritableDatabase();
        }
    }

    public static DataBaseMaster getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseMaster(context);
        }
        return instance;
    }

    public void addUser(User user) {
        ContentValues cv = new ContentValues();
        cv.put(DataBaseCreator.UserColums.NAME, user.name);
        database.insert(DataBaseCreator.UserColums.TABLE_NAME, null, cv);
        //TODO DataBaseCreator.UserColums  alt+enter and do static, and after i use just TABLE_Name and others
        Log.d("addUser", database.insert(DataBaseCreator.UserColums.TABLE_NAME, null, cv) + "");


    }

    public List<User> getAllUsersRAW() {
        String query = " SELECT * FROM " +
                DataBaseCreator.UserColums.TABLE_NAME;
        Log.d("getAllUsers", "quey: " + query);
        Cursor cursor = database.rawQuery(query, null);

        List<User> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User user = new User();
            user.name = cursor.getString(cursor.getColumnIndex(DataBaseCreator.UserColums.NAME));
            Log.d("getAllUsers", "name: " + user.name);
            list.add(user);
            cursor.moveToNext();

        }
        cursor.close();
        return list;
    }

    public List<User> getAllUsersQuery() {

        Cursor cursor = database.query(DataBaseCreator.UserColums.TABLE_NAME, new String[]{DataBaseCreator.UserColums.NAME}, null, null,
                null, null, null);

        List<User> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User user = new User();
            user.name = cursor.getString(cursor.getColumnIndex(DataBaseCreator.UserColums.NAME));
            Log.d("getAllUsers", "name: " + user.name);
            list.add(user);
            cursor.moveToNext();

        }
        cursor.close();
        return list;
    }
}
