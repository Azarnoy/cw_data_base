package com.example.azzarnoy.cw_bd.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Анна on 27.02.2016.
 */
public class DataBaseCreator extends SQLiteOpenHelper {

    public static final String DB_NAME = "db_name";
    public static final int DB_VERSION = 1;

    public static class UserColums implements BaseColumns {
        public static final String TABLE_NAME = "users";
        public static final String NAME = "name";
    }

    private static String CREATE_TABLE_USER = "CREATE TABLE " + UserColums.TABLE_NAME +
            " (" +
            UserColums._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            UserColums.NAME + " TEXT" +
            ");";

    public DataBaseCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USER);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
